#!/bin/bash

set_user_info() {
    USER_ID=$(grep -i "Requestor's uid:" full.txt | cut -d ":" -f 2 | awk '{$1=$1};1')
    PRIMARY_UNIT=$(grep "primary unit" full.txt | cut -d ":" -f 2- | jq -r ".prefix")
    # NOTE: There are two formats for the Admins MCommunity Group in the request form,
    # so this code searches for both.
    grep -iq "Admins MCommunity group:" full.txt &&
        ADMINS_MCOMMUNITY_GROUP=$(grep -i "Admins MCommunity group:" full.txt | cut -d ":" -f 2 | awk '{$1=$1};1')
    grep -iq "MCommunity Group Name:" full.txt &&
        ADMINS_MCOMMUNITY_GROUP=$(grep -i "MCommunity Group Name:" full.txt | cut -d ":" -f 2 | awk '{$1=$1};1' | head -n 1)
}

set_storage_volume_needs() {

    read -r -p "Do you need Data Den [Y/n] " input
    case $input in
        [yY][eE][sS]|[yY])
            DATADEN=true
            ;;
        [nN][oO]|[nN])
            DATADEN=false
            ;;
        *)
            echo "Invalid input..."
            exit 1
            ;;
    esac

    read -r -p "Do you need Turbo Non Sensitive? [Y/n] " input
    case $input in
        [yY][eE][sS]|[yY])
            TURBO_NONSENSITIVE=true
            ;;
        [nN][oO]|[nN])
            TURBO_NONSENSITIVE=false
            ;;
        *)
            echo "Invalid input..."
            exit 1
            ;;
    esac

    read -r -p "Do you need Turbo Sensitive? [Y/n] " input
    case $input in
        [yY][eE][sS]|[yY])
            TURBO_SENSITIVE=true
            ;;
        [nN][oO]|[nN])
            TURBO_SENSITIVE=false
            ;;
        *)
            echo "Invalid input..."
            exit 1
            ;;
    esac
}

set_user_info
set_storage_volume_needs

echo $AD_GROUP_NAME
echo $GID

echo $DATADEN_BLOCK

if [ "$DATADEN" = true ]; then
    INPUT_FILE="dataden.txt"
    VOLUME_NAME=$(grep -i "DataDen volume name:" ${INPUT_FILE} | cut -d ":" -f 2 | awk '{$1=$1};1')
    VOLUME_MCOMMUNITY_GROUP=$(grep -i "mcommunity group:" ${INPUT_FILE} | cut -d ":" -f 2 | awk '{$1=$1};1')
    USERS=$(grep -i "DataDen users:" ${INPUT_FILE} | cut -d ":" -f 2- | awk '{$1=$1};1')
    read -r -p "Enter GID for ${VOLUME_MCOMMUNITY_GROUP}: " GID
    #echo "Please create the following Data Den volume - ${PRIMARY_UNIT}-${VOLUME_NAME}"
    echo "Please create the following Data Den volume - ${VOLUME_NAME}"
    echo "UID = ${USER_ID}"
    echo "Notification group = ${ADMINS_MCOMMUNITY_GROUP}"
    echo "GID = ${GID}"
    #echo "Volume name = ${PRIMARY_UNIT}-${VOLUME_NAME}"
    echo "Volume name = ${VOLUME_NAME}"
    echo "AD group/owner = ${VOLUME_MCOMMUNITY_GROUP}"
    echo "Size = 100T"
    echo "Shortcode = 123578"
    echo "Users = ${USERS}"
    echo
fi

if [ "$TURBO_NONSENSITIVE" = true ]; then
    INPUT_FILE="turbo.txt"
    VOLUME_NAME=$(grep -i "Turbo volume name:" ${INPUT_FILE} | cut -d ":" -f 2 | awk '{$1=$1};1')
    VOLUME_SIZE=$(grep -i "Turbo volume size:" ${INPUT_FILE} | cut -d ":" -f 2 | awk '{$1=$1};1')
    VOLUME_MCOMMUNITY_GROUP=$(grep -i "mcommunity group:" ${INPUT_FILE} | cut -d ":" -f 2 | awk '{$1=$1};1')

    # Build list of hosts for export
    HOST_LIST=""
    for SYSTEM in GreatLakes Lighthouse Globus NonArc; do
        grep -i "Export ${SYSTEM}" ${INPUT_FILE} | grep -q true && HOST_LIST="${HOST_LIST} ${SYSTEM}"
    done
    # Trim leading space and replace spaces between hosts with commas
    HOST_LIST=$(echo ${HOST_LIST} | sed 's/ *$//g' | sed 's/ /,/g')

    USERS=$(grep -i "Turbo users:" ${INPUT_FILE} | cut -d ":" -f 2- | awk '{$1=$1};1')
    IS_SENSITIVE=$(grep -i "Is sensitive:" ${INPUT_FILE} | cut -d ":" -f 2- | awk '{$1=$1};1')
    read -r -p "Enter GID for ${VOLUME_MCOMMUNITY_GROUP}: " GID
    #echo "Please create the following (non-sensitive)Turbo volume - ${PRIMARY_UNIT}-${VOLUME_NAME}"
    echo "Please create the following (non-sensitive)Turbo volume - ${VOLUME_NAME}"
    echo "UID = ${USER_ID}"
    echo "Notification group = ${ADMINS_MCOMMUNITY_GROUP}"
    echo "GID = ${GID}"
    if [ $IS_SENSITIVE == "No" ]; then
        echo "Regulated/Sensitive = No"
    else
        echo "ERROR"
        exit 1
    fi
    echo "Volume name = ${VOLUME_NAME}"
    echo "Basic + Snapshots + Replication"
    echo "Size = ${VOLUME_SIZE}T"
    echo "Multi-protocol = true"
    echo "AD group/owner = ${VOLUME_MCOMMUNITY_GROUP}"
    echo "Hosts = ${HOST_LIST}"
    echo "Shortcode = 123150"
    echo "Users = ${USERS}"
    echo
fi

if [ "$TURBO_SENSITIVE" = true ]; then
    INPUT_FILE="turbosensitive.txt"
    VOLUME_NAME=$(grep -i "Turbo volume name:" ${INPUT_FILE} | cut -d ":" -f 2 | awk '{$1=$1};1')
    VOLUME_SIZE=$(grep -i "Turbo volume size:" ${INPUT_FILE} | cut -d ":" -f 2 | awk '{$1=$1};1')
    VOLUME_MCOMMUNITY_GROUP=$(grep -i "mcommunity group:" ${INPUT_FILE} | cut -d ":" -f 2 | awk '{$1=$1};1')

    # Build list of hosts for export
    HOST_LIST=""
    for SYSTEM in Armis Globus NonArc; do
        grep -i "Export ${SYSTEM}" ${INPUT_FILE} | grep -q true && HOST_LIST="${HOST_LIST} ${SYSTEM}"
    done
    # Trim leading space and replace spaces between hosts with commas
    HOST_LIST=$(echo ${HOST_LIST} | sed 's/ *$//g' | sed 's/ /,/g')

    USERS=$(grep -i "Turbo users:" ${INPUT_FILE} | cut -d ":" -f 2- | awk '{$1=$1};1')
    IS_SENSITIVE=$(grep -i "Is sensitive:" ${INPUT_FILE} | cut -d ":" -f 2- | awk '{$1=$1};1')
    read -r -p "Enter GID for ${VOLUME_MCOMMUNITY_GROUP}: " GID
    #echo "Please create the following (sensitive)Turbo volume - ${PRIMARY_UNIT}-${VOLUME_NAME}"
    echo "Please create the following (sensitive)Turbo volume - ${VOLUME_NAME}"
    echo "UID = ${USER_ID}"
    echo "Notification group = ${ADMINS_MCOMMUNITY_GROUP}"
    echo "GID = ${GID}"
    if [ $IS_SENSITIVE == "Yes" ]; then
        echo "Regulated/Sensitive = Yes"
    else
        echo "ERROR"
        exit 1
    fi
    #echo "Volume name = ${PRIMARY_UNIT}-${VOLUME_NAME}"
    echo "Volume name = ${VOLUME_NAME}"
    echo "Basic + Snapshots + Replication"
    echo "Size = ${VOLUME_SIZE}T"
    echo "Multi-protocol = true"
    echo "AD group/owner = ${VOLUME_MCOMMUNITY_GROUP}"
    echo "Hosts = ${HOST_LIST}"
    echo "Shortcode = 123150"
    echo "Users = ${USERS}"
    echo
fi
