## Usage
- Clean the contents of all local files with `./clean.sh`.
- Copy the full content of UMRCP request into `full.txt`.
- Copy the contents of each storage volume into `dataden.txt`, `turbo.txt`, and `turbosensitive.txt`.
- Run `./srs-request.sh`

## Example

```bash
(base) ~/workspace/umrcp-scripts  (master) $ ./clean.sh
(base) ~/workspace/umrcp-scripts  (master) $ vi full.txt
(base) ~/workspace/umrcp-scripts  (master) $ vi dataden.txt
(base) ~/workspace/umrcp-scripts  (master) $ ./srs-request.sh
Do you need Data Den [Y/n] y
Do you need Turbo Non Sensitive? [Y/n] n
Do you need Turbo Sensitive? [Y/n] n



Enter GID for umms-dnoll-dataden: 2452194
Please create the following Data Den volume - umms-dnoll
UID = 108370
Notification group = arcts-dnoll-admins
GID = 2452194
Volume name = umms-dnoll
AD group/owner = umms-dnoll-dataden
Size = 100T
Shortcode = 123578
Users = dnoll,shoucguo

(base) ~/workspace/umrcp-scripts  (master) $
```
